﻿namespace PDFtoExcel
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.btn_Load = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.txt_Source = new System.Windows.Forms.RichTextBox();
            this.cmb_Ratio = new System.Windows.Forms.ComboBox();
            this.lbl_Accuracy = new System.Windows.Forms.Label();
            this.btn_Save = new System.Windows.Forms.Button();
            this.dtg_Output = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtg_Output)).BeginInit();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // btn_Load
            // 
            this.btn_Load.Location = new System.Drawing.Point(40, 35);
            this.btn_Load.Name = "btn_Load";
            this.btn_Load.Size = new System.Drawing.Size(75, 23);
            this.btn_Load.TabIndex = 0;
            this.btn_Load.Text = "Load PDF";
            this.btn_Load.UseVisualStyleBackColor = true;
            this.btn_Load.Click += new System.EventHandler(this.btn_Load_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitContainer1.Location = new System.Drawing.Point(0, 82);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.txt_Source);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dtg_Output);
            this.splitContainer1.Size = new System.Drawing.Size(800, 368);
            this.splitContainer1.SplitterDistance = 404;
            this.splitContainer1.TabIndex = 2;
            // 
            // txt_Source
            // 
            this.txt_Source.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt_Source.Location = new System.Drawing.Point(0, 0);
            this.txt_Source.Name = "txt_Source";
            this.txt_Source.Size = new System.Drawing.Size(404, 368);
            this.txt_Source.TabIndex = 0;
            this.txt_Source.Text = "";
            // 
            // cmb_Ratio
            // 
            this.cmb_Ratio.FormattingEnabled = true;
            this.cmb_Ratio.Location = new System.Drawing.Point(255, 35);
            this.cmb_Ratio.Name = "cmb_Ratio";
            this.cmb_Ratio.Size = new System.Drawing.Size(121, 21);
            this.cmb_Ratio.TabIndex = 3;
            this.cmb_Ratio.SelectedIndexChanged += new System.EventHandler(this.cmb_Ratio_SelectedIndexChanged);
            // 
            // lbl_Accuracy
            // 
            this.lbl_Accuracy.AutoSize = true;
            this.lbl_Accuracy.Location = new System.Drawing.Point(169, 38);
            this.lbl_Accuracy.Name = "lbl_Accuracy";
            this.lbl_Accuracy.Size = new System.Drawing.Size(52, 13);
            this.lbl_Accuracy.TabIndex = 4;
            this.lbl_Accuracy.Text = "Accuracy";
            // 
            // btn_Save
            // 
            this.btn_Save.Location = new System.Drawing.Point(556, 33);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(81, 23);
            this.btn_Save.TabIndex = 6;
            this.btn_Save.Text = "Save";
            this.btn_Save.UseVisualStyleBackColor = true;
            this.btn_Save.Click += new System.EventHandler(this.btn_Save_Click);
            // 
            // dtg_Output
            // 
            this.dtg_Output.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtg_Output.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtg_Output.Location = new System.Drawing.Point(0, 0);
            this.dtg_Output.Name = "dtg_Output";
            this.dtg_Output.Size = new System.Drawing.Size(392, 368);
            this.dtg_Output.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btn_Save);
            this.Controls.Add(this.lbl_Accuracy);
            this.Controls.Add(this.cmb_Ratio);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.btn_Load);
            this.Name = "Form1";
            this.Text = "PDF To Excel";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtg_Output)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Button btn_Load;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ComboBox cmb_Ratio;
        private System.Windows.Forms.Label lbl_Accuracy;
        private System.Windows.Forms.Button btn_Save;
        private System.Windows.Forms.RichTextBox txt_Source;
        private System.Windows.Forms.DataGridView dtg_Output;
    }
}

