﻿using System;
using System.Text;
using System.Windows.Forms;
using System.IO;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using System.Collections.Generic;
using System.Linq;
using SpreadsheetLight;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Data;

namespace PDFtoExcel
{
    public partial class Form1 : Form
    {
        List<List<string>> output;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var slicers = new List<double> { 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7 };
            foreach (var item in slicers)
                cmb_Ratio.Items.Add(item);
            cmb_Ratio.SelectedIndex = 2;
        }

        private string PDFtoExcel(string filename)
        {
            string text = "";
            try
            {
                PdfReader reader = new PdfReader(filename);
                for (int page = 1; page <= reader.NumberOfPages; page++)
                {
                    ITextExtractionStrategy its = new iTextSharp.text.pdf.parser.LocationTextExtractionStrategy();
                    String s = PdfTextExtractor.GetTextFromPage(reader, page, its);

                    s = Encoding.UTF8.GetString(ASCIIEncoding.Convert(Encoding.Default, Encoding.UTF8, Encoding.Default.GetBytes(s)));
                    text = text + s;

                }
                reader.Close();
                return text;
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.Message);
            }
            return null;
        }

        public SLDocument AddSheet(SLDocument doc, string sheetname, List<List<string>> list)
        {
            doc.AddWorksheet(sheetname);
            for (int i = 0; i < list.Count; i++)
            {
                for (int j = 0; j < list[i].Count; j++)
                {
                    doc.SetCellValue(i + 1, j + 1, list[i][j]);
                }
            }
            return doc;
        }

        private void btn_Load_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "PDF Files (*.pdf)|*.pdf";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string text = PDFtoExcel(openFileDialog1.FileName);
                txt_Source.Text = text;
                txt_Source.RightMargin = 9000;
                Slicingtext(text);
            }
        }

        private void Slicingtext(string text)
        {
            fillOutput(text);
        }

        private void cmb_Ratio_SelectedIndexChanged(object sender, EventArgs e)
        {
            double slicer = (double)(cmb_Ratio.SelectedIndex + 1) / 10.0;
            string text = txt_Source.Text;
            fillOutput(text);
        }

        private void fillOutput(string text)
        {
            List<string> textSeparated = text.Split('\n').ToList();
            List<double> ratio = new List<double>();
            textSeparated.ForEach(t => ratio.Add((double)t.Count(Char.IsWhiteSpace) / (double)t.Length));
            output = new List<List<string>>();
            double slicer = (double)(cmb_Ratio.SelectedIndex + 1) / 10.0;

            for (int i = 0; i < ratio.Count; i++)
                if (ratio[i] > slicer && ratio[i] < 1)
                    output.Add(Regex.Split(textSeparated[i], @"[\s\t]{2,}", RegexOptions.IgnorePatternWhitespace).ToList());

            SLDocument doc = new SLDocument();
            doc = AddSheet(doc, "Main", output);
            DataTable dt = new DataTable();
            dt = BuildDatatable(doc);
            dtg_Output.DataSource = dt;
        }

        private void btn_Save_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "Excel Files (*.xlsx)|*.xlsx";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                SLDocument doc = new SLDocument();
                doc = AddSheet(doc, "Main", output);
                doc.DeleteWorksheet("Sheet1");
                doc.SaveAs(saveFileDialog1.FileName);
                Process.Start(saveFileDialog1.FileName);
            }
        }

        private DataTable BuildDatatable(SLDocument doc)
        {
            var dt = new DataTable();
            var rows = new List<List<string>>();

            for (int i = 1; i <= doc.GetWorksheetStatistics().EndRowIndex; i++)
            {
                var row = new List<string>();
                for (int j = 1; j <= doc.GetWorksheetStatistics().EndColumnIndex; j++)
                    row.Add(doc.GetCellValueAsString(i, j).ToString());
                rows.Add(row);
            }
            if (rows.Any())
            {
                foreach (var columnName in rows.First())
                    if (dt.Columns.Contains(columnName))
                        dt.Columns.Add($"{columnName}({(dt.Columns.Cast<DataColumn>().Count(col => col.ColumnName == columnName || (col.ColumnName.LastIndexOf(columnName) == 0 && col.ColumnName.Contains('('))).ToString())})");
                    else dt.Columns.Add(columnName);
                rows.Skip(1).ToList().ForEach(row => dt.Rows.Add(row.ToArray()));
            }
            return dt;
        }
    }
}
